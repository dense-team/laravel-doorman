# User authentication via database for Lavarel

Simple package extending laravel auth via database.

## About

This package relies on dense/baseraiser package that is essentialy a wrapper for doctrine DBAL package. Another package that it is required is dense\enum package which is just few simple classes for easier enumerations handling. 
Baseraiser package implements database repository pattern so it is not suitable for eloquent implementation.

## Instalation

Run artisan command
```shell
php artisan doorman:generate
```
This command will generate model and database handling class in App\Model\User directory.  

Run following composer command.
```shell
composer require dense/doorman
```

Additional changes must be made to RegisterController and ResetPasswordController.
  
RegisterController
```php
use Illuminate\Support\Facades\App;
use App\Model\User\User;
use App\Model\User\UserBase;
  
/**
 * Get a validator for an incoming registration request.
 *
 * @param  array  $data
 * @return \Illuminate\Contracts\Validation\Validator
 */
protected function validator(array $data)
{
    return Validator::make($data, [
        'forename' => ['required', 'string', 'max:255'],
        'surname' => ['required', 'string', 'max:255'],
        'email' => ['required', 'string', 'email', 'max:255'],
        'password' => ['required', 'string', 'min:8', 'confirmed'],
    ]);
}
  
/**
 * Create a new user instance after a valid registration.
 *
 * @param  array  $data
 * @return \Illuminate\Contracts\Auth\Authenticatable
 */
protected function create(array $data)
{
    $allowedData = [
        'forename',
        'surname',
        'email',
    ];
  
    $user = new User();
    $user->hydrate(array_intersect_key($data, array_flip($allowedData)));
    $user->setAuthPassword($data['password']);
  
    $userBase = App::make(UserBase::class);
    $user = $userBase->create($user);
  
    return $user;
}

/**
 * The user has been registered.
 *
 * @param  \Illuminate\Http\Request  $request
 * @param  mixed  $user
 * @return mixed
 */
protected function registered(Request $request, $user)
{
    if ($user->hasInactiveStatus()) {
        $this->guard()->logout();
    }
}
```
  
ResetPasswordController
```php
use App\Model\User\UserBase;
  
/**
 * Reset the given user's password.
 *
 * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
 * @param  string  $password
 * @return void
 */
protected function resetPassword($user, $password)
{
    $user->password = Hash::make($password);
  
    $user->setRememberToken(Str::random(60));
  
    $userBase = App::make(UserBase::class);
    $userBase->save($user);
  
    event(new PasswordReset($user));
  
    $this->guard()->login($user);
}
```

## Notifications
To enable email notification after successfull user registration set the following code in the App\Providers\EventServiceProvider.
```
/**
 * The subscriber classes to register.
 *
 * @var array
 */
protected $subscribe = [
    'Dense\Doorman\Registration\Subscribers\RegistrationSubscriber',
];
```
  
To enable email notification after successfull user activation set the following code in the App\Providers\EventServiceProvider.
```
/**
 * The subscriber classes to register.
 *
 * @var array
 */
protected $subscribe = [
    'Dense\Doorman\User\Subscribers\ActivationSubsriber',
];

## Configuration

If you are running lumen you need to add following lines to bootstrap/app.php file.
```
$app->register(\Dense\Doorman\DoormanServiceProvider::class);
```
