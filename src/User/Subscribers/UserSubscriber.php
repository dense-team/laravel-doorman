<?php

namespace Dense\Doorman\User\Subscribers;

use Illuminate\Support\Facades\Mail;

use Dense\Doorman\Mail\ActivationNotification;

class UserSubscriber
{
    /**
     * Handle user register events.
     */
    public function onUserActivate($event) {
        $address = $event->user->email;

        Mail::to($address)
            ->send(new ActivationNotification($event->user));
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'Dense\Doorman\User\Events\ActivateUser',
            'Dense\Doorman\User\Subscribers\UserSubscriber@onUserActivate'
        );
    }
}
