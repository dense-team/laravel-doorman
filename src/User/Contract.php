<?php

namespace Dense\Doorman\User;

use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

interface Contract extends AuthenticatableContract, CanResetPasswordContract
{
    /**
     * @return bool
     */
    public function isAdmin();

    /**
     * @return bool
     */
    public function isNotAdmin();

    /**
     * @return bool
     */
    public function isRegular();

    /**
     * @return bool
     */
    public function isNotRegular();

    /**
     * @return bool
     */
    public function hasActiveStatus();

    /**
     * @return bool
     */
    public function hasInactiveStatus();

    /**
     * @return string
     */
    public function getName();

    /**
     * @return void
     */
    public function generateApiToken();

    /**
     * @return bool
     */
    public function hasApiToken();
}
