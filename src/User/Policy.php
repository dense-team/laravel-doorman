<?php

namespace Dense\Doorman\User;

use Illuminate\Auth\Access\HandlesAuthorization;
//use Illuminate\Contracts\Auth\Authenticatable;

use Dense\Doorman\User\Contract as UserContract;

class Policy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param \Dense\Doorman\User\Contract $user
     * @return bool
     */
    public function before(UserContract $user)
    {
        if ($user->isAdmin()) {
            return true;
        }
    }

    /**
     * @param \Dense\Doorman\User\Contract $user
     * @return bool
     */
    public function view(UserContract $user)
    {
        return false;
    }

    /**
     * @param \Dense\Doorman\User\Contract $user
     * @return bool
     */
    public function edit(UserContract $user)
    {
        return false;
    }

    /**
     * @param \Dense\Doorman\User\Contract $user
     * @return bool
     */
    public function delete(UserContract $user)
    {
        return false;
    }
}
