<?php

namespace Dense\Doorman\User\Events;

use Illuminate\Queue\SerializesModels;

use Dense\Doorman\User\Contract as UserContract;

class ActivateUser
{
    use SerializesModels;

    /**
     * The user.
     *
     * @var \Dense\Doorman\User\Contract
     */
    public $user;

    /**
     * Create a new event instance.
     *
     * @param  \Dense\Doorman\User\Contract  $user
     * @return void
     */
    public function __construct(UserContract $user)
    {
        $this->user = $user;
    }
}
