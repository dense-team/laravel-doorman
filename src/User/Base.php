<?php

namespace Dense\Doorman\User;

use Illuminate\Contracts\Auth\Authenticatable;

use Dense\Baseraiser\Table\EntityTable;
use Dense\Enum\Status;

class Base extends EntityTable
{
    const FILTER_USER_ID = 'user_id';

    const SORTER_NAME = 'name';
    const SORTER_SURNAME = 'surname';
    const SORTER_FORENAME = 'forename';
    const SORTER_IDENTIFIER = 'identifier';

    /**
     * @return string
     */
    protected function table()
    {
        return 'users';
    }

    /**
     * @return string
     */
    protected function index()
    {
        return 'user_id';
    }

    /**
     * @param string $email
     * @return \Illuminate\Support\Collection
     * @throws \Exception
     */
    public function findByEmail($email)
    {
        $data = $this->select($this->row())
            ->from($this->table())
            ->where("email = :email")
            ->setParameter('email', $email)
            ->execute()
            ->fetchAll();

        if (!$data) {
            throw new \Exception('Položka neexistuje');
        }

        return $this->getResult($data);
    }

    /**
     * @param array $credentials
     * @return \Illuminate\Support\Collection
     * @throws \Exception
     */
    public function findByCredentials(array $credentials)
    {
        unset($credentials['password']);
        unset($credentials['password_confirmation']);

        $base = $this->select($this->row())
            ->from($this->table());

        foreach ($credentials as $col => $value) {
            $base
                ->andWhere("$col = :$col")
                ->setParameter($col, $value);
        }

        $data = $base->execute()
            ->fetchAll();

        if (!$data) {
            throw new \Exception('Položka neexistuje');
        }

        return $this->getResult($data);
    }

    /**
     * @param \Illuminate\Contracts\Auth\Authenticatable $user
     * @param string $token
     * @return \Illuminate\Contracts\Auth\Authenticatable
     */
    public function modifyRememberToken(Authenticatable $user, $token)
    {
        $this->update($this->table())
            ->set('remember_token', ':remember_token')
            ->where("{$this->index()} = :id")
            ->setParameter('remember_token', $token)
            ->setParameter('id', $user->getAuthIdentifier())
            ->execute();

        return $user;
    }

    /**
     * @param \Illuminate\Contracts\Auth\Authenticatable $user
     * @return \Illuminate\Contracts\Auth\Authenticatable
     */
    public function activate(Authenticatable $user)
    {
        $this->update($this->table())
            ->set('status', ':status')
            ->where("{$this->index()} = :id")
            ->setParameter('status', Status::STATUS_ACTIVE)
            ->setParameter('id', $user->getAuthIdentifier())
            ->execute();

        return $user;
    }
}
