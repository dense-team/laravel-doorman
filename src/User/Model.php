<?php

namespace Dense\Doorman\User;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Notifications\Notifiable;

use Dense\Doorman\User\Contract as UserContract;
use Dense\Enum\Role as UserRole;
use Dense\Enum\Status as UserStatus;

class Model implements UserContract
{
    use CanResetPassword, Notifiable;

    /**
     * @var int
     */
    public $user_id;

    /**
     * @var string
     */
    public $identifier;

    /**
     * @var string
     */
    public $forename;

    /**
     * @var string
     */
    public $surname;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $phone;

    /**
     * @var string
     */
    public $password;

    /**
     * @var string
     */
    public $api_token;

    /**
     * @var string
     */
    public $status = UserStatus::STATUS_INACTIVE;

    /**
     * @var string
     */
    public $role = UserRole::ROLE_REGULAR;

    /**
     * @var string
     */
    public $remember_token;

    /**
     * @var string
     */
    public $email_verified_at;

    /**
     * @var string
     */
    public $read_at;

    /**
     * @var string
     */
    public $created_at;

    /**
     * @param array $data
     * @return $this
     */
    public function hydrate(array $data)
    {
        if (array_key_exists('user_id', $data)) {
            $this->user_id = (int)$data['user_id'];
        }
        if (array_key_exists('identifier', $data)) {
            $this->identifier = $data['identifier'] ?? null;
        }
        if (array_key_exists('forename', $data)) {
            $this->forename = $data['forename'];
        }
        if (array_key_exists('surname', $data)) {
            $this->surname = $data['surname'];
        }
        if (array_key_exists('email', $data)) {
            $this->email = $data['email'];
        }
        if (array_key_exists('phone', $data)) {
            $this->phone = $data['phone'] ?? null;
        }
        if (array_key_exists('password', $data)) {
            $this->password = $data['password'];
        }
        if (array_key_exists('api_token', $data)) {
            $this->api_token = $data['api_token'] ?? null;
        }
        if (array_key_exists('status', $data)) {
            $this->status = $data['status'];
        }
        if (array_key_exists('role', $data)) {
            $this->role = $data['role'];
        }

        // laravel properties
        if (array_key_exists('remember_token', $data)) {
            $this->remember_token = $data['remember_token'];
        }
        if (array_key_exists('email_verified_at', $data)) {
            $this->email_verified_at = $data['email_verified_at'] ? date('Y-m-d H:i:s', strtotime($data['email_verified_at'])) : null;
        }
        if (array_key_exists('read_at', $data)) {
            $this->read_at = $data['read_at'] ? date('Y-m-d H:i:s', strtotime($data['read_at'])) : null;
        }
        if (array_key_exists('created_at', $data)) {
            $this->created_at = date('Y-m-d H:i:s', strtotime($data['created_at']));
        }

        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $data = [
            'user_id' => $this->user_id,
            'identifier' => $this->identifier,
            'forename' => $this->forename,
            'surname' => $this->surname,
            'email' => $this->email,
            'phone' => $this->phone,
            'password' => $this->password,
            'api_token' => $this->api_token,
            'status' => $this->status,
            'role' => $this->role,

            // laravel properties
            'email_verified_at' => $this->email_verified_at,
            'read_at' => $this->read_at,
            'created_at' => $this->created_at,
        ];

        return $data;
    }

    /**
     * @return bool
     */
    public function isAdmin()
    {
        return $this->role === UserRole::ROLE_ADMIN;
    }

    /**
     * @return bool
     */
    public function isNotAdmin()
    {
        return !$this->isAdmin();
    }

    /**
     * @return bool
     */
    public function isRegular()
    {
        return $this->role === UserRole::ROLE_REGULAR;
    }

    /**
     * @return bool
     */
    public function isNotRegular()
    {
        return !$this->isRegular();
    }

    /**
     * @return bool
     */
    public function hasActiveStatus()
    {
        return $this->status === UserStatus::STATUS_ACTIVE;
    }

    /**
     * @return bool
     */
    public function hasInactiveStatus()
    {
        return $this->status === UserStatus::STATUS_INACTIVE;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return trim(ucfirst(mb_strtolower($this->forename)) . ' ' . ucfirst(mb_strtolower($this->surname)));
    }

    /**
     * @return string
     */
    public function getAuthIdentifierName()
    {
        return 'user_id';
    }

    /**
     * @return int
     */
    public function getAuthIdentifier()
    {
        return $this->user_id;
    }

    /**
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setAuthPassword($value)
    {
        $this->password = Hash::make($value);

        return $this;
    }

    /**
     * @return string
     */
    public function getRememberToken()
    {
        return $this->remember_token;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setRememberToken($value)
    {
        $this->remember_token = $value ?? null;

        return $this;
    }

    /**
     * @return string
     */
    public function getRememberTokenName()
    {
        return 'remember_token';
    }

    /**
     * @return $this
     */
    public function generateApiToken()
    {
        $this->api_token = Str::random(60);

        return $this;
    }

    /**
     * @return bool
     */
    public function hasApiToken()
    {
        return !is_null($this->api_token);
    }
}
