<?php

namespace Dense\Doorman\Commands;

use Illuminate\Console\Command;
use Illuminate\Console\DetectsApplicationNamespace;

class Generate extends Command
{
    use DetectsApplicationNamespace;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'doorman:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates user model classes.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $modelDirectory = app_path('Model/User');
        if (!is_dir($modelDirectory)) {
            mkdir($modelDirectory, 0755, true);
        }

        file_put_contents(
            $modelDirectory . '/User.php',
            $this->compileStub(__DIR__ . '/stubs/user/model.stub')
        );

        file_put_contents(
            $modelDirectory . '/UserBase.php',
            $this->compileStub(__DIR__ . '/stubs/user/base.stub')
        );
    }

    protected function compileStub($path)
    {
        return str_replace(
            '{{namespace}}',
            $this->getAppNamespace(),
            file_get_contents($path)
        );
    }
}
