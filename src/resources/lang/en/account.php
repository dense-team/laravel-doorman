<?php

return [
    'user' => 'User',
    'name' => 'Name',
    'forename' => 'Name',
    'surname' => 'Surname',
    'email' => 'Email',
    'phone' => 'Phone',
    'password' => 'Password',
    'password_confirmation' => 'Password confirmation',
    'login' => 'Login',
    'logout' => 'Logout',
    'registration' => 'Registration',
    'role' => 'Role',
    'status' => 'Status',
    'api_token' => 'API token',
    'generate_token' => 'Generate API token',
];
