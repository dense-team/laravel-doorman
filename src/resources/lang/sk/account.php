<?php

return [
    'user' => 'Používateľ',
    'name' => 'Meno',
    'forename' => 'Meno',
    'surname' => 'Priezvisko',
    'email' => 'Email',
    'phone' => 'Telefón',
    'password' => 'Heslo',
    'password_confirmation' => 'Overenie hesla',
    'login' => 'Prihlásenie',
    'logout' => 'Odhlásenie',
    'registration' => 'Registrácia',
    'role' => 'Úloha',
    'status' => 'Stav',
    'api_token' => 'API token',
    'generate_token' => 'Generovať API token',
];
