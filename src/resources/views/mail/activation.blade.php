@component('mail::message')

Your account <strong>{{ $user->getName() }}</strong> has just been activated on site {{ config('app.name') }}.
<br />
Start using our website by clicking the following link: <a href="{{ route('login') }}">{{ config('app.name') }}</a>.

@endcomponent
