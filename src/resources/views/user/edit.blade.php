@extends('layouts.app')

@section('heading')
    {{ __('model.user') }}
    @if($user->user_id)
        {{ $user->getName() }}
    @endif
@endsection

@section('content')
    @include('assistant::report.all')

    <form method="post" action="{{ route('user.edit', ['user_id' => $user->user_id]) }}">
        @csrf

        <div class="form-group row">
            <label for="forename" class="col-md-4 col-form-label text-md-right">{{ __('doorman::account.forename') }}</label>
            <div class="col-md-6">
                <input id="forename" type="text" class="form-control{{ $errors->has('forename') ? ' is-invalid' : '' }}" name="forename" value="{{ old('forename', $user->forename) }}" required autofocus>

                @include('assistant::validation.field', ['field' => 'forename'])
            </div>
        </div>

        <div class="form-group row">
            <label for="surname" class="col-md-4 col-form-label text-md-right">{{ __('doorman::account.surname') }}</label>
            <div class="col-md-6">
                <input id="surname" type="text" class="form-control{{ $errors->has('surname') ? ' is-invalid' : '' }}" name="surname" value="{{ old('surname', $user->surname) }}" required>

                @include('assistant::validation.field', ['field' => 'surname'])
            </div>
        </div>

        <div class="form-group row">
            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('doorman::account.email') }}</label>
            <div class="col-md-6">
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email', $user->email) }}" required>

                @include('assistant::validation.field', ['field' => 'email'])
            </div>
        </div>

        <div class="form-group row">
            <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('doorman::account.phone') }}</label>
            <div class="col-md-6">
                <input id="phone" type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ old('phone', $user->phone) }}">

                @include('assistant::validation.field', ['field' => 'phone'])
            </div>
        </div>

        <div class="form-group row">
            <label for="role" class="col-md-4 col-form-label text-md-right">{{ __('doorman::account.role') }}</label>
            <div class="col-md-6">
                <select id="role" class="form-control{{ $errors->has('role') ? ' is-invalid' : '' }}"
                        name="role" required>
                    <option value="">{{ __('form.select_placeholder') }}</option>
                    @foreach(\Dense\Enum\Role::getEnums() as $roleId => $roleTitle)
                        <option value="{{ $roleId }}" {{ old('role', $user->role) == $roleId ? 'selected' : '' }}>
                            {{ $roleTitle }}
                        </option>
                    @endforeach
                </select>

                @include('assistant::validation.field', ['field' => 'role'])
            </div>
        </div>

        <div class="form-group row">
            <label for="status" class="col-md-4 col-form-label text-md-right">{{ __('doorman::account.status') }}</label>
            <div class="col-md-6">
                <select id="status" class="form-control{{ $errors->has('status') ? ' is-invalid' : '' }}"
                        name="status" required>
                    <option value="">{{ __('form.select_placeholder') }}</option>
                    @foreach(\Dense\Enum\Status::getEnums() as $statusId => $statusTitle)
                        <option value="{{ $statusId }}" {{ old('status', $user->status) == $statusId ? 'selected' : '' }}>
                            {{ $statusTitle }}
                        </option>
                    @endforeach
                </select>

                @include('assistant::validation.field', ['field' => 'status'])
            </div>
        </div>

        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                <button type="submit" id="plupload-submit" class="btn btn-success btn-stateful">
                    <span class="material-icons">save</span>
                    <span class="align-middle">{{ __('general.save') }}</span>
                </button>
            </div>
        </div>
    </form>
@endsection
