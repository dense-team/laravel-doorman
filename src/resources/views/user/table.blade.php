@extends('layouts.app')

@section('heading')
    {{ __('model.user_list') }}
@endsection

@section('content')
    @include('assistant::report.all')

    @can('edit', \Dense\Doorman\User\Contract::class)
    <p>
        <a href="{{ route('user.edit') }}" class="btn btn-primary">
            <span class="material-icons">add</span>
            <span class="align-middle">{{ __('general.add') }}</span>
        </a>
    </p>
    @endcan

    @if(!$users->isEmpty())
        <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                    <th style="width: 50px;">#</th>
                    <th>{{ __('doorman::account.name') }}</th>
                    <th>{{ __('doorman::account.email') }}</th>
                    <th>{{ __('doorman::account.role') }}</th>
                    <th>{{ __('doorman::account.status') }}</th>
                    <th style="width: 130px;"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($users->values() as $idx => $user)
                    <tr>
                        <td>{{ $idx + 1 }}</td>
                        <td>{{ $user->getName() }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ \Dense\Enum\Role::getEnum($user->role) }}</td>
                        <td>{{ \Dense\Enum\Status::getEnum($user->status) }}</td>
                        <td class="no-wrap">
                            @can('edit', \Dense\Doorman\User\Contract::class)
                                <a class="btn btn-success" href="{{ route('user.edit', ['user_id' => $user->user_id]) }}" title="{{ __('general.edit') }}">
                                    <span class="material-icons">edit</span>
                                </a>
                            @endcan
                            @can('delete', \Dense\Doorman\User\Contract::class)
                                <a class="btn btn-danger" href="{{ route('user.delete', ['user_id' => $user->user_id]) }}" title="{{ __('general.delete') }}" @confirmDelete>
                                    <span class="material-icons">delete</span>
                                </a>
                            @endcan
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @endif
@endsection
