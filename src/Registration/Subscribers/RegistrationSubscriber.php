<?php

namespace Dense\Doorman\Registration\Subscribers;

use Illuminate\Support\Facades\Mail;

use Dense\Doorman\Mail\RegistrationNotification;

class RegistrationSubscriber
{
    /**
     * Handle user register events.
     */
    public function onUserRegister($event) {
        $address = config('doorman.email');

        Mail::to($address)
            ->send(new RegistrationNotification($event->user));
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'Illuminate\Auth\Events\Registered',
            'Dense\Doorman\Registration\Subscribers\RegistrationSubscriber@onUserRegister'
        );
    }
}
