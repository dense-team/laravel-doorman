<?php

namespace Dense\Doorman\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

use App\Http\Controllers\Controller as BaseController;

use App\Model\User\UserBase;

class AccountController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function account(
        Request $request,
        UserBase $userBase
    ) {
        $user = Auth::user();

        if ($request->isMethod('post')) {
            $validation = [
                'forename' => ['required', 'string', 'max:255'],
                'surname' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255'],
                'password' => ['nullable', 'string', 'min:8', 'confirmed'],
            ];

            $this->validate($request, $validation);

            $user->hydrate($request->except(['user_id', 'password', 'role', 'type']));
            if ($request->filled('password')) {
                $user->setAuthPassword($request->input('password'));
            }

            try {
                $userBase->save($user);

                return Redirect::route('account')
                    ->withSuccess('Uloženie prebehlo úspešne');
            } catch (\Exception $e) {
                $this->sendException($e);

                return Redirect::back()
                    ->withInput()
                    ->withFail('Uloženie zlyhalo');
            }
        }

        return View::make('doorman::account.account', [
            'user' => $user,
        ]);
    }

    public function token(
        UserBase $userBase
    ) {
        $user = Auth::user();

        $user->generateApiToken();

        try {
            $userBase->save($user);

            return Redirect::route('account')
                ->withSuccess('API token bol vygenerovaný úspešne');
        } catch (\Exception $e) {
            $this->sendException($e);

            return Redirect::back()
                ->withInput()
                ->withFail('Generovanie zlyhalo');
        }
    }
}
