<?php

namespace Dense\Doorman\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

use Dense\Doorman\User\Contract as UserContract;
use Dense\Doorman\User\Events\ActivateUser;

use App\Http\Controllers\Controller as BaseController;

use App\Model\User\UserBase;
use App\Model\User\User;

class UserController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function table(
        UserBase $userBase
    ) {
        $this->authorize('view', UserContract::class);

        $users = $userBase
            ->addSorter(UserBase::SORTER_NAME)
            ->all();

        return View::make('doorman::user.table', [
            'users' => $users,
        ]);
    }

    public function edit(
        Request $request,
        UserBase $userBase,
        $userId = null
    ) {
        $this->authorize('edit', UserContract::class);

        if ($userId) {
            try {
                $user = $userBase->find($userId)->first();
            } catch (\Exception $e) {
                return Redirect::route('user.get')
                    ->withFail('Požadovaná položka neexistuje');
            }
        } else {
            $user = new User();
        }

        if ($request->isMethod('post')) {
            $validation = [
                'forename' => ['required', 'string', 'max:255'],
                'surname' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255'],
                'status' => ['required', 'string', 'max:50'],
            ];

            $this->validate($request, $validation);

            $user->hydrate($request->except(['user_id']));

            try {
                $userBase->save($user);

                return Redirect::route('user.table')
                    ->withSuccess('Uloženie prebehlo úspešne');
            } catch (\Exception $e) {
                $this->sendException($e);

                return Redirect::back()
                    ->withInput()
                    ->withFail('Uloženie zlyhalo');
            }
        }

        return View::make('doorman::user.edit', [
            'user' => $user,
        ]);
    }

    public function delete(
        UserBase $userBase,
        $userId
    ) {
        $this->authorize('delete', UserContract::class);

        try {
            $user = $userBase->find($userId)->first();
        } catch (\Exception $e) {
            return Redirect::route('user.table')
                ->withFail('Požadovaná položka neexistuje');
        }

        try {
            $userBase->remove($user);

            return Redirect::route('user.table')
                ->withSuccess('Odstránenie prebehlo úspešne');
        } catch (\Exception $e) {
            $this->sendException($e);

            return Redirect::route('user.table')
                ->withFail('Odstránenie zlyhalo');
        }
    }

    public function activate(
        UserBase $userBase,
        $userId
    ) {
        try {
            $user = $userBase->find($userId)->first();
        } catch (\Exception $e) {
            return Redirect::route('user.get')
                ->withFail('Požadovaná položka neexistuje');
        }

        try {
            $userBase->activate($user);

            event(new ActivateUser($user));

            return Redirect::route('user.table')
                ->withSuccess('Uloženie prebehlo úspešne');
        } catch (\Exception $e) {
            $this->sendException($e);

            return Redirect::back()
                ->withInput()
                ->withFail('Uloženie zlyhalo');
        }
    }
}
