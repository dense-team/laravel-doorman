<?php

namespace Dense\Doorman;

use Illuminate\Support\ServiceProvider;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

use Dense\Doorman\Commands\Generate;

use Dense\Doorman\Auth\Provider as UserProvider;
use Dense\Doorman\User\Contract as UserContract;
use Dense\Doorman\User\Policy as UserPolicy;

use App\Model\User\UserBase;
use App\Model\User\User;

class DoormanServiceProvider extends ServiceProvider
{
    /**
     * @var string
     */
    protected $namespace = 'doorman';

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Auth::provider('dense', function ($app) {
            $userBase = $app->make(UserBase::class);

            return new UserProvider($userBase);
        });

        Gate::policy(UserContract::class, UserPolicy::class);

        // commands
        $this->commands([
            Generate::class,
        ]);

        // routes
        $this->loadRoutesFrom(__DIR__ . '/routes/doorman.php');

        // config
        $this->publishes([
            __DIR__ . '/config/doorman.php' => config_path('doorman.php'),
        ]);

        $this->mergeConfigFrom(__DIR__ . '/config/doorman.php', 'doorman');

        // langs
        $this->loadTranslationsFrom(__DIR__ . '/resources/lang', $this->namespace);

        $this->publishes([
            __DIR__ . '/resources/lang' => resource_path('lang/vendor/doorman'),
        ]);

        // views
        $this->loadViewsFrom(__DIR__ . '/resources/views', $this->namespace);

        $this->publishes([
            __DIR__ . '/resources/views' => resource_path('views/vendor/doorman'),
        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UserBase::class, function() {
            return new UserBase(new User());
        });
    }
}
