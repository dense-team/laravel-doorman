<?php

namespace Dense\Doorman\Auth;

use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Hash;

use Dense\Enum\Status;

use Dense\Doorman\User\Base;

class Provider implements UserProvider
{
    /**
     * @var \Dense\Doorman\User\Base
     */
    protected $userBase;

    /**
     * Provider constructor.
     * @param \Dense\Doorman\User\Base $userBase
     */
    public function __construct(Base $userBase)
    {
        $this->userBase = $userBase;
    }

    /**
     * @param int $identifier
     * @return \Illuminate\Support\Collection
     */
    public function retrieveById($identifier)
    {
        try {
            $user = $this->userBase->find($identifier)
                ->first();

            return $user;
        } catch (\Exception $e) {
        }
    }

    /**
     * @param array $credentials
     * @return \Illuminate\Support\Collection
     */
    public function retrieveByCredentials(array $credentials)
    {
        $credentials = array_merge($credentials, [
            'status' => Status::STATUS_ACTIVE,
        ]);

        try {
            $user = $this->userBase->findByCredentials($credentials)
                ->first();

            return $user;
        } catch (\Exception $e) {
        }
    }

    /**
     * @param int $identifier
     * @param string $token
     * @return \Illuminate\Support\Collection
     */
    public function retrieveByToken($identifier, $token)
    {
        try {
            $user = $this->userBase->find($identifier)
                ->first();

            if (hash_equals($user->getRememberToken(), $token)) {
                return $user;
            }
        } catch (\Exception $e) {
        }
    }

    /**
     * @param \Illuminate\Contracts\Auth\Authenticatable $user
     * @param string $token
     * @return \Illuminate\Contracts\Auth\Authenticatable
     */
    public function updateRememberToken(Authenticatable $user, $token)
    {
        try {
            return $this->userBase->modifyRememberToken($user, $token);
        } catch (\Exception $e) {
        }
    }

    /**
     * @param \Illuminate\Contracts\Auth\Authenticatable $user
     * @param array $credentials
     * @return bool
     */
    public function validateCredentials(Authenticatable $user, array $credentials)
    {
        return Hash::check($credentials['password'], $user->getAuthPassword());
    }

    /**
     * @param \Illuminate\Contracts\Auth\Authenticatable $user
     * @param array $credentials
     * @param bool $force
     */
    public function rehashPasswordIfRequired(Authenticatable $user, array $credentials, bool $force = false)
    {
        // later
    }
}
