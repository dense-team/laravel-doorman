<?php

namespace Dense\Doorman\Mail;

use Illuminate\Mail\Mailable;

class RegistrationNotification extends Mailable
{
    /**
     * @var \Dense\Doorman\User\Contract;
     */
    public $user;

    /**
     * Create a new message instance.
     *
     * @var \Dense\Doorman\User\Contract $user;
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = config('app.name') . ' - ' .  __('Registration notification');

        return $this
            ->subject($subject)
            ->markdown('doorman::mail.registration');
    }
}
